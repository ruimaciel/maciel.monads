using System;

namespace Maciel.Monads
{

    /*
    Inspired by a very ancient but still very inspiring blog post on Monads
    "The Marvels of Monads", 2018-01-10
    https://blogs.msdn.microsoft.com/wesdyer/2008/01/10/the-marvels-of-monads/
     */
    public class Maybe<T>
    {

        public T Value { get; private set; }

        public bool HasValue { get; private set; }

        Maybe()
        {
            HasValue = false;
        }

        public Maybe(T value)
        {
            Value = value;
            HasValue = true;
        }

        public Maybe<U> Bind<U>(Func<T, Maybe<U>> f)
        {
            return f(Value);
        }

        public readonly static Maybe<T> Nothing = new Maybe<T>();

    }

}
