using System;

namespace Maciel.Monads
{
    public class Success<TSuccess>
    {

        public TSuccess Value { get; private set; }

        public Success(TSuccess value)
        {
            Value = value;
        }

        public Success<USuccess> Bind<USuccess>(Func<Success<TSuccess>, Success<USuccess>> f)
        {
            return f(this);
        }

    }

    public class Failure<TFailure>
    {

        public TFailure Value { get; private set; }

        public Failure(TFailure value)
        {
            Value = value;
        }

        public Failure<UFailure> Bind<UFailure>(Func<Failure<TFailure>, Failure<UFailure>> f)
        {
            return f(this);
        }
    }

    public class Result<TFailure, TSuccess>
    {

        private readonly TFailure m_failure;
        private readonly TSuccess m_success;

        public bool IsFailure { get; private set; }

        public bool IsSuccess => !IsFailure;

        public Failure<TFailure> Failure => IsFailure ? new Failure<TFailure>(m_failure) : null;
        public Success<TSuccess> Success => IsSuccess ? new Success<TSuccess>(m_success) : null;

        public Result(Failure<TFailure> failure)
        {
            IsFailure = true;
            m_failure = failure.Value;
        }

        public Result(Success<TSuccess> success)
        {
            IsFailure = false;
            m_success = success.Value;
        }

        public static Result<TFailure, TSuccess> MakeFailure(TFailure value)
        {
            return new Result<TFailure, TSuccess>(new Failure<TFailure>(value));
        }

        public static Result<TFailure, TSuccess> MakeSuccess(TSuccess value)
        {
            return new Result<TFailure, TSuccess>(new Success<TSuccess>(value));
        }

        public static implicit operator Result<TFailure, TSuccess>(Failure<TFailure> failure)
        {
            return new Result<TFailure, TSuccess>(failure);
        }

        public static implicit operator Result<TFailure, TSuccess>(Success<TSuccess> success)
        {
            return new Result<TFailure, TSuccess>(success);
        }

        public Result<UFailure, USuccess> Bind<UFailure, USuccess>(Func<Result<TFailure, TSuccess>, Result<UFailure, USuccess>> f)
        {
            return f(this);
        }

        // Inspired in Rust's Result type:
        // https://doc.rust-lang.org/nightly/core/result/enum.Result.html#method.map
        public Result<TFailure, USuccess> Map<USuccess>(Func<TSuccess, Result<TFailure, USuccess>> f)
        {
            if (IsFailure)
            {
                return Failure;
            }

            return f(Success.Value);
        }

        // Inspired in Rust's Result type:
        // https://doc.rust-lang.org/nightly/core/result/enum.Result.html#method.map_error
        public Result<UFailure, TSuccess> MapError<UFailure>(Func<TFailure, Result<UFailure, TSuccess>> f)
        {
            if( IsSuccess)
            {
                return Success;
            }

            return f(Failure.Value);
        }

        // Inspired in Rust's Result type:
        // https://doc.rust-lang.org/nightly/core/result/enum.Result.html#method.and_then
        public Result<TFailure, TSuccess> AndThen(Func<TSuccess, Result<TFailure, TSuccess>> f)
        {
            if (IsFailure)
            {
                return Failure;
            }

            return f(Success.Value);
        }


        // Inspired in Rust's Result type:
        // https://doc.rust-lang.org/nightly/core/result/enum.Result.html#method.or_else
        public Result<TFailure, TSuccess> OrElse(Func<TFailure, Result<TFailure, TSuccess>> f)
        {
            if (IsSuccess)
            {
                return Success;
            }

            return f(Failure.Value);
        }

    }

}
