﻿using System;

namespace Maciel.Monads
{

    public class Left<TLeft>
    {

        public TLeft Value { get; private set; }

        public Left(TLeft value)
        {
            Value = value;
        }

        public Left<ULeft> Bind<ULeft>(Func<Left<TLeft>, Left<ULeft>> f)
        {
            return f(this);
        }

    }

    public class Right<TRight>
    {

        public TRight Value { get; private set; }

        public Right(TRight value)
        {
            Value = value;
        }

        public Right<URight> Bind<URight>(Func<Right<TRight>, Right<URight>> f)
        {
            return f(this);
        }
    }

    public class Either<TLeft, TRight>
    {

        private readonly TLeft m_left;
        private readonly TRight m_right;

        public bool IsLeft { get; private set; }

        public bool IsRight => !IsLeft;

        public Left<TLeft> Left => IsLeft ? new Left<TLeft>(m_left) : null;
        public Right<TRight> Right => IsRight ? new Right<TRight>(m_right) : null;

        public Either(Left<TLeft> left)
        {
            IsLeft = true;
            m_left = left.Value;
        }

        public Either(Right<TRight> right)
        {
            IsLeft = false;
            m_right = right.Value;
        }

        public static implicit operator Either<TLeft, TRight>(Left<TLeft> left)
        {
            return new Either<TLeft, TRight>(left);
        }

        public static implicit operator Either<TLeft, TRight>(Right<TRight> right)
        {
            return new Either<TLeft, TRight>(right);
        }

        public Either<ULeft, URight> Bind<ULeft, URight>(Func<Either<TLeft, TRight>, Either<ULeft, URight>> f)
        {
            return f(this);
        }

    }
}
