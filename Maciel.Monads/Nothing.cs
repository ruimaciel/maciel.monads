using System;

namespace Maciel.Monads
{
    public class Nothing : IEquatable<Nothing>
    {
        public Nothing()
        { }

        public bool Equals(Nothing other)
        {
            if(other == null) return false;

            return true;
        }
    }
}
