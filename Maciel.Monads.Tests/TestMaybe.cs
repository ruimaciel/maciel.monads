using System;
using Xunit;

namespace Maciel.Monads.Tests
{
    public class TestMaybe
    {
        
        [Fact]
        public void Test_Given_Value_When_Initialize_Then_MaybeHasValueIsTrue()
        {
            //Given
            bool value = true;

            //When
            var maybe = new Maybe<bool>(value);

            //Then
            Assert.True(maybe.HasValue);
        }

        [Fact]
        public void Test_Given_Nothing_When_Initialize_Then_MaybeHasValueIsFalse()
        {
            //Given

            //When
            var maybe = Maybe<bool>.Nothing;

            //Then
            Assert.False(maybe.HasValue);
        }

        [Fact]
        public void Test_LeftIdentity()
        {
            //Given
            int inputValue = 2;

            Func<int, Maybe<int>> f = b => new Maybe<int>(2*b);

            //When
            var maybe = new Maybe<int>(inputValue).Bind(f);

            //Then
            Assert.Equal(maybe.Value, f(inputValue).Value);
        }

        [Fact]
        public void Test_RightIdentity()
        {
            //Given
            int inputValue = 2;

            //When
            var m = new Maybe<int>(inputValue);

            //Then
            Assert.Equal(m.Bind(x => new Maybe<int>(x)).Value, m.Value);
        }

    }
}