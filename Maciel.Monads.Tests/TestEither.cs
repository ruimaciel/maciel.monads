using System;
using Xunit;

namespace Maciel.Monads.Tests
{
    public class TestEither
    {
        [Fact]
        public void Test_Given_LeftBool_When_Initialize_Then_LeftValueIsEqual()
        {
            //Given
            bool leftValue = true;

            //When
            var left = new Left<bool>(leftValue);

            //Then
            Assert.Equal(left.Value, leftValue);
        }

        [Fact]
        public void Test_Given_LeftBool_When_ExplicitlyConvertLeftBoolToEitherBoolInt_Then_EitherIsLeftIsTrue()
        {
            //Given
            bool leftValue = true;
            var left = new Left<bool>(leftValue);

            //When
            Either<bool, int> either = (Either<bool, int>)left;

            //Then
            Assert.True(either.IsLeft);
        }

        [Fact]
        public void Test_Given_LeftBool_When_ImplicitlyConvertLeftBoolToEitherBoolInt_Then_EitherIsLeftIsTrue()
        {
            //Given
            bool leftValue = true;
            var left = new Left<bool>(leftValue);

            //When
            Either<bool, int> either = left;

            //Then
            Assert.True(either.IsLeft);
        }

        [Fact]
        public void Test_Given_LeftBool_When_ExplicitlyConvertLeftBoolToEitherBoolInt_Then_EitherIsRightIsFalse()
        {
            //Given
            bool leftValue = true;
            var left = new Left<bool>(leftValue);

            //When
            Either<bool, int> either = (Either<bool, int>)left;

            //Then
            Assert.False(either.IsRight);
        }

        [Fact]
        public void Test_Given_LeftBool_When_ImplicitlyConvertLeftBoolToEitherBoolInt_Then_EitherIsRightIsFalse()
        {
            //Given
            bool leftValue = true;
            var left = new Left<bool>(leftValue);

            //When
            Either<bool, int> either = left;

            //Then
            Assert.False(either.IsRight);
        }

        [Fact]
        public void Test_Given_RightBool_When_ExplicitlyConvertRightBoolToEitherBoolInt_Then_EitherIsRightIsTrue()
        {
            //Given
            int rightValue = 42;
            var right = new Right<int>(rightValue);

            //When
            Either<bool, int> either = (Either<bool, int>)right;

            //Then
            Assert.True(either.IsRight);
        }

        [Fact]
        public void Test_Given_RightBool_When_ImplicitlyConvertRightBoolToEitherBoolInt_Then_EitherIsRightIsTrue()
        {
            //Given
            int rightValue = 42;
            var right = new Right<int>(rightValue);

            //When
            Either<bool, int> either = right;

            //Then
            Assert.True(either.IsRight);
        }

        [Fact]
        public void Test_Given_RightBool_When_ExplicitlyConvertRightBoolToEitherBoolInt_Then_EitherIsLeftIsFalse()
        {
            //Given
            int rightValue = 42;
            var right = new Right<int>(rightValue);

            //When
            Either<bool, int> either = (Either<bool, int>)right;

            //Then
            Assert.False(either.IsLeft);
        }

        [Fact]
        public void Test_Given_RightBool_When_ImplicitlyConvertRightBoolToEitherBoolInt_Then_EitherIsLeftIsFalse()
        {
            //Given
            int rightValue = 42;
            var right = new Right<int>(rightValue);

            //When
            Either<bool, int> either = right;

            //Then
            Assert.False(either.IsLeft);
        }

        [Fact]
        public void Test_Given_RightValue_When_Bind_Then_OutputIsRight()
        {
            //Given
            int rightValue = 42;
            Either<bool, int> either = new Right<int>(rightValue);

            //When
            Func<Either<bool, int>, Either<bool, int>> f = (input) =>
            {
                return input.IsLeft ?
                    new Either<bool, int>(new Left<bool>(input.Left.Value)) :
                    new Either<bool, int>(new Right<int>(input.Right.Value));
            };

            var output = either.Bind(f);

            //Then
            Assert.True(output.IsRight);
        }

        [Fact]
        public void Test_Given_RightValue_When_Bind_Then_OutputEqualsFunctionAppliedToValue()
        {
            //Given
            int rightValue = 42;

            Either<bool, int> either = new Right<int>(rightValue);

            //When
            Func<Either<bool, int>, Either<bool, int>> f = (input) =>
            {
                return input.IsLeft ?
                    new Either<bool, int>(new Left<bool>(input.Left.Value)) :
                    new Either<bool, int>(new Right<int>(input.Right.Value));
            };

            var output = either.Bind(f);

            //Then
            Assert.Equal(output.Right.Value, rightValue);
        }

        [Fact]
        public void Test_Given_SameLeftAndRightType_When_InstantiateLeft_Then_EitherIsLeftOutputIsTrue()
        {
            //Given
            int value = 42;

            //When
            Either<int, int> either = new Left<int>(value);

            //Then
            Assert.True(either.IsLeft);
        }

        [Fact]
        public void Test_Given_SameLeftAndRightType_When_InstantiateLeft_Then_EitherIsRightOutputIsFalse()
        {
            //Given
            int value = 42;

            //When
            Either<int, int> either = new Left<int>(value);

            //Then
            Assert.False(either.IsRight);
        }

        [Fact]
        public void Test_Given_SameLeftAndRightType_When_InstantiateRight_Then_EitherIsLeftOutputIsFalse()
        {
            //Given
            int value = 42;

            //When
            Either<int, int> either = new Right<int>(value);

            //Then
            Assert.False(either.IsLeft);
        }

        [Fact]
        public void Test_Given_SameLeftAndRightType_When_InstantiateRight_Then_EitherIsRightOutputIsTrue()
        {
            //Given
            int value = 42;

            //When
            Either<int, int> either = new Right<int>(value);

            //Then
            Assert.True(either.IsRight);
        }
    }
}
