using System;
using Xunit;

namespace Maciel.Monads.Tests
{
    public class TestResult
    {
        [Fact]
        public void Test_Given_FailureBool_When_Initialize_Then_FailureValueIsEqual()
        {
            //Given
            bool leftValue = true;

            //When
            var left = new Failure<bool>(leftValue);

            //Then
            Assert.Equal(left.Value, leftValue);
        }

        [Fact]
        public void Test_Given_FailureBool_When_ExplicitlyConvertFailureBoolToResultBoolInt_Then_ResultIsFailureIsTrue()
        {
            //Given
            bool leftValue = true;
            var left = new Failure<bool>(leftValue);

            //When
            Result<bool, int> either = (Result<bool, int>)left;

            //Then
            Assert.True(either.IsFailure);
        }

        [Fact]
        public void Test_Given_FailureBool_When_ImplicitlyConvertFailureBoolToResultBoolInt_Then_ResultIsFailureIsTrue()
        {
            //Given
            bool leftValue = true;
            var left = new Failure<bool>(leftValue);

            //When
            Result<bool, int> either = left;

            //Then
            Assert.True(either.IsFailure);
        }

        [Fact]
        public void Test_Given_FailureBool_When_ExplicitlyConvertFailureBoolToResultBoolInt_Then_ResultIsSuccessIsFalse()
        {
            //Given
            bool leftValue = true;
            var left = new Failure<bool>(leftValue);

            //When
            Result<bool, int> either = (Result<bool, int>)left;

            //Then
            Assert.False(either.IsSuccess);
        }

        [Fact]
        public void Test_Given_FailureBool_When_ImplicitlyConvertFailureBoolToResultBoolInt_Then_ResultIsSuccessIsFalse()
        {
            //Given
            bool leftValue = true;
            var left = new Failure<bool>(leftValue);

            //When
            Result<bool, int> either = left;

            //Then
            Assert.False(either.IsSuccess);
        }

        [Fact]
        public void Test_Given_SuccessBool_When_ExplicitlyConvertSuccessBoolToResultBoolInt_Then_ResultIsSuccessIsTrue()
        {
            //Given
            int rightValue = 42;
            var right = new Success<int>(rightValue);

            //When
            Result<bool, int> either = (Result<bool, int>)right;

            //Then
            Assert.True(either.IsSuccess);
        }

        [Fact]
        public void Test_Given_SuccessBool_When_ImplicitlyConvertSuccessBoolToResultBoolInt_Then_ResultIsSuccessIsTrue()
        {
            //Given
            int rightValue = 42;
            var right = new Success<int>(rightValue);

            //When
            Result<bool, int> either = right;

            //Then
            Assert.True(either.IsSuccess);
        }

        [Fact]
        public void Test_Given_SuccessBool_When_ExplicitlyConvertSuccessBoolToResultBoolInt_Then_ResultIsFailureIsFalse()
        {
            //Given
            int rightValue = 42;
            var right = new Success<int>(rightValue);

            //When
            Result<bool, int> either = (Result<bool, int>)right;

            //Then
            Assert.False(either.IsFailure);
        }

        [Fact]
        public void Test_Given_SuccessBool_When_ImplicitlyConvertSuccessBoolToResultBoolInt_Then_ResultIsFailureIsFalse()
        {
            //Given
            int rightValue = 42;
            var right = new Success<int>(rightValue);

            //When
            Result<bool, int> either = right;

            //Then
            Assert.False(either.IsFailure);
        }

        [Fact]
        public void Test_Given_SuccessValue_When_Bind_Then_OutputIsSuccess()
        {
            //Given
            int successValue = 42;
            Result<bool, int> either = new Success<int>(successValue);

            //When
            Func<Result<bool, int>, Result<bool, int>> f = (input) =>
            {
                return input.IsFailure ?
                    Result<bool, int>.MakeFailure(input.Failure.Value) :
                    Result<bool, int>.MakeSuccess(input.Success.Value);
            };

            var output = either.Bind(f);

            //Then
            Assert.True(output.IsSuccess);
        }

        [Fact]
        public void Test_Given_SuccessValue_When_Bind_Then_OutputEqualsFunctionAppliedToValue()
        {
            //Given
            int rightValue = 42;

            Result<bool, int> either = new Success<int>(rightValue);

            //When
            Func<Result<bool, int>, Result<bool, int>> f = (input) =>
            {
                return input.IsFailure ?
                    Result<bool, int>.MakeFailure(input.Failure.Value) :
                    Result<bool, int>.MakeSuccess(input.Success.Value);
            };

            var output = either.Bind(f);

            //Then
            Assert.Equal(output.Success.Value, rightValue);
        }

        [Fact]
        public void Test_Given_SameFailureAndSuccessType_When_InstantiateFailure_Then_ResultIsFailureOutputIsTrue()
        {
            //Given
            int value = 42;

            //When
            Result<int, int> either = new Failure<int>(value);

            //Then
            Assert.True(either.IsFailure);
        }

        [Fact]
        public void Test_Given_SameFailureAndSuccessType_When_InstantiateFailure_Then_ResultIsSuccessOutputIsFalse()
        {
            //Given
            int value = 42;

            //When
            Result<int, int> either = new Failure<int>(value);

            //Then
            Assert.False(either.IsSuccess);
        }

        [Fact]
        public void Test_Given_IntValue_When_MapToFloatAndMapToInt_Then_ResultIsSucccessOutputIsEqualToValue()
        {
            //Given
            int value = 42;
            Result<bool, int> a = new Success<int>(value);

            //When
            var result = a
                .Map(x => Result<bool, float>.MakeSuccess(x))
                .Map(x => Result<bool, int>.MakeSuccess((int)x));

            //Then
            Assert.True(result.IsSuccess);
            Assert.IsType<int>(result.Success.Value);
            Assert.Equal(value, result.Success.Value);
        }

        [Fact]
        public void Test_Given_IntValue_When_MapToFloatWithFailureFollowedByMapToInt_Then_ResultIsFailure()
        {
            //Given
            int value = 42;
            Result<bool, int> a = new Success<int>(value);

            //When
            var result = a
                .Map(x => Result<bool, float>.MakeFailure(false))
                .Map(x => Result<bool, int>.MakeSuccess(44));

            //Then
            Assert.True(result.IsFailure);
            Assert.IsType<bool>(result.Failure.Value);
            Assert.False(result.Failure.Value);
        }

        [Fact]
        public void Test_Given_InitialSuccess_When_AndThenFollowedByAndThen_Then_ResultIsSuccess()
        {
            //Given
            int value = 42;
            Result<bool, int> a = new Success<int>(value);

            //When
            var result = a
                .AndThen(x => Result<bool, int>.MakeSuccess(43))
                .AndThen(x => Result<bool, int>.MakeSuccess(44));

            //Then
            Assert.True(result.IsSuccess);
            Assert.IsType<int>(result.Success.Value);
            Assert.Equal(44, result.Success.Value);
        }

    }
}
