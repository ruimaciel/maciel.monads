using Xunit;

namespace Maciel.Monads.Tests
{
    public class TestNothing
    {
        [Fact]
        public void Test_When_CompareSameNothingObject_Then_ComparisonIsTrue()
        {
            //Given
            var lhs = new Nothing();

            //When

            //Then
            Assert.Equal(lhs, lhs);
        }

        [Fact]
        public void Test_When_CompareDifferentNothingObjects_Then_ComparisonIsTrue()
        {
            //Given
            var lhs = new Nothing();
            var rhs = new Nothing();

            //When

            //Then
            Assert.Equal(lhs, rhs);
        }
    }
}