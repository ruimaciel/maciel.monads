# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] 2023-11-16
### Changed
- Migrates project to .NET 8

## [0.4.0] 2019-09-01
### Added
- `Result.Map()` and `Result.MapError()`
- `Result.AndThen()` and `Result.OrElse()`

## [0.3.0] 2019-06-29
### Added
- `Nothing` class
- `Response.MakeFailure()` and `Response.MakeSuccess()`

## [0.2.0] 2019-05-22
### Added
- `Maybe` monad
- `Either.Bind()`
- `Result` monad

## [0.1.0] 2019-05-12
- Initial release
